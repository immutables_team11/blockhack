/**
 * this is a logic for  sending the delivery request.
 * @param {org.awesomnetwork.CreateLocalPolicy} globalPolicyData
 * @transaction
 */

async function createNewLocalPolicy(globalPolicyData) {
  console.log(globalPolicyData);
  var factory = getFactory();
  var NS = "org.awesomnetwork";
  if (globalPolicyData.owner) {
    console.log(globalPolicyData);
    var localPolicyId = new Date().getUTCMilliseconds().toString();
    return getAssetRegistry(NS + ".LocalProductSpec")
      .then(function(localpolicyAssetRegistry) {
        // Get the factory for creating new asset instances.
        var factory = getFactory();
        // Create the vehicle.
        var localPolicy = factory.newResource(
          NS,
          "LocalProductSpec",
          localPolicyId
        );
        localPolicy.globalProductData = globalPolicyData.globalProduct;
        localPolicy.localRegion = globalPolicyData.localRegion;
        localPolicy.localEligibility = globalPolicyData.localEligibility;
        localPolicy.localPremium = globalPolicyData.localPremium;
        localPolicy.localCurrency = globalPolicyData.localCurrency;
        localPolicy.localLanguage = globalPolicyData.localLanguage;
        // Add the vehicle to the vehicle asset registry.
        return localpolicyAssetRegistry.add(localPolicy);
      })
      .catch(function(error) {
        // Add optional error handling here.
      });
  } else {
    console.log(globalPolicyData, "this is the data");
  }
}

/**
 * this is a logic for  sending the delivery request.
 * @param {org.awesomnetwork.SendMonthlyStatNotification} notificationData
 * @transaction
 */

async function sendNotificationMsgToGlobalOrg(notificationData) {
  console.log(notificationData);
  var factory = getFactory();
  var NS = "org.awesomnetwork";
  // Get the vehicle asset registry.
  // Get the factory for creating new asset instances.
  var http = new XMLHttpRequest();
  var url = "https://api.sandbox.transferwise.tech/v1/quotes";
  http.open("POST", url, true);
  //Send the proper header information along with the request
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.setRequestHeader(
    "AUTHORIZATION",
    "Bearer 1385e04b-54f1-49fd-8654-856b29fb35e4"
  );

  http.onreadystatechange = function() {
    //Call a function when the state changes.
    if (http.readyState == 4 && http.status == 200) {
      var responceData = JSON.parse(http.response);
      
    }
  };
  var sendData = {
    id: 4365,
    source: "AUD",
    target: "EUR",
    targetAmount: 100.0,
    type: "BALANCE_PAYOUT",
    rate: 0.9073,
    rateType: "FIXED",
    fee: 2.34,
    allowedProfileTypes: ["PERSONAL", "BUSINESS"]
  };
  http.send(JSON.stringify(sendData));

  return getAssetRegistry(NS + ".MonthlyStatNotification")
    .then(function(monthlyStatNotificationRegistry) {
      var factory = getFactory();
      var mothlyNotificationId = new Date().getUTCMilliseconds().toString();
      // Create the vehicle.
      var notification = factory.newResource(
        NS,
        "MonthlyStatNotification",
        mothlyNotificationId
      );
      notification.localProduct = notificationData.localProduct;
      notification.noOfpolicySold = notificationData.noOfpolicySold;
      notification.totalPremiumOweToParent =
        notificationData.totalPremiumOweToParent;
      notification.currentMonthPremiumDispercement =
        notificationData.currentMonthPremiumDispercement;
      notification.transanctionInetiationDate =
        notificationData.transanctionInetiationDate;
      notification.modeOfTransanction = notificationData.modeOfTransanction;
      notification.senderAccountDetails = notificationData.senderAccountDetails;
      notification.reciverAccountDetails =
        notificationData.reciverAccountDetails;
      // Add the vehicle to the vehicle asset registry.
      return monthlyStatNotificationRegistry.add(notification);
    })
    .catch(function(error) {
      // Add optional error handling here.
    });
}

/**
 * this is a logic for  sending the delivery request.
 * @param {org.awesomnetwork.SendMonthlyStat} monthlyStatData
 * @transaction
 */

async function sendMoneyToGlobalOrg(monthlyStatData) {
  console.log(notificationData);

  var http = new XMLHttpRequest();
  var url = "https://api.sandbox.transferwise.tech/v1/quotes";
  http.open("POST", url, true);
  //Send the proper header information along with the request
  http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  http.setRequestHeader(
    "AUTHORIZATION",
    "Bearer 1385e04b-54f1-49fd-8654-856b29fb35e4"
  );
  http.onreadystatechange = function() {
    //Call a function when the state changes.
    if (http.readyState == 4 && http.status == 200) {
      console.log(JSON.parse(http.response), "http here");
    }
  };
  var sendData = {
    id: 4365,
    source: "EUR",
    target: "GBP",
    sourceAmount: 663.84,
    targetAmount: 600.0,
    type: "BALANCE_PAYOUT",
    rate: 0.9073,
    rateType: "FIXED",
    fee: 2.34,
    allowedProfileTypes: ["PERSONAL", "BUSINESS"]
  };
  http.send(JSON.stringify(sendData));

  var factory = getFactory();
  var NS = "org.awesomnetwork";
  // Get the vehicle asset registry.
  // Get the factory for creating new asset instances.
  return getAssetRegistry(NS + ".MonthlyStatSent")
    .then(function(monthlyStatSentRegistry) {
      var factory = getFactory();
      var mothlyNotificationId = new Date().getUTCMilliseconds().toString();
      // Create the vehicle.
      var notification = factory.newResource(
        NS,
        "MonthlyStatSent",
        mothlyNotificationId
      );
      notification.localProduct = notificationData.localProduct;
      notification.noOfpolicySold = notificationData.noOfpolicySold;
      notification.totalPremiumOweToParent =
        notificationData.totalPremiumOweToParent;
      notification.currentMonthPremiumDispercement =
        notificationData.currentMonthPremiumDispercement;
      notification.transanctionInetiationDate =
        notificationData.transanctionInetiationDate;
      notification.modeOfTransanction = notificationData.modeOfTransanction;
      notification.senderAccountDetails = notificationData.senderAccountDetails;
      notification.reciverAccountDetails =
        notificationData.reciverAccountDetails;
      // Add the vehicle to the vehicle asset registry.
      return monthlyStatNotificationRegistry.add(notification);
    })
    .catch(function(error) {
      // Add optional error handling here.
    });
}
