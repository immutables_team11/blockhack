/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Buyyer permissions
rule BuyyerTimberPurchaseOrderPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: DELETE,UPDATE
    resource: "org.timberchain.TimberPurchaseOrder"
    action: DENY
}

rule BuyyerDeliveryRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: ALL
    resource: "org.timberchain.DeliveryRequest"
    action: DENY
}

rule BuyyerTimberPurchaseRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseRequest"
    action: DENY
}

rule BuyyerPurchaseOrdersInvoicePermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: ALL
    resource: "org.timberchain.PurchaseOrdersInvoice"
    action: DENY
}

rule BuyyerMarshelerRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: ALL
    resource: "org.timberchain.MarshelerRequest"
    action: DENY
}

rule BuyyerTimberRequestStatusPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: ALL
    resource: "org.timberchain.TimberRequestStatus"
    action: DENY
}

rule BuyyerTimbeterReadingPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: ALL
    resource: "org.timberchain.TimbeterReading"
    action: DENY
}

rule BuyyerTruckerDataPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Buyer"
    operation: ALL
    resource: "org.timberchain.TruckerData"
    action: DENY
}


// Logger permissions
rule LoggerTimberPurchaseOrderPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseOrder"
    action: DENY
}

rule LoggerDeliveryRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: UPDATE, DELETE
    resource: "org.timberchain.DeliveryRequest"
    action: DENY
}

rule LoggerTimberPurchaseRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseRequest"
    action: DENY
}

rule LoggerPurchaseOrdersInvoicePermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: ALL
    resource: "org.timberchain.PurchaseOrdersInvoice"
    action: DENY
}

rule LoggerMarshelerRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: ALL
    resource: "org.timberchain.MarshelerRequest"
    action: DENY
}

rule LoggerTimberRequestStatusPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: ALL
    resource: "org.timberchain.TimberRequestStatus"
    action: DENY
}

rule LoggerTimbeterReadingPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: DELETE
    resource: "org.timberchain.TimbeterReading"
    action: DENY
}

rule LoggerTruckerDataPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Logger"
    operation: ALL
    resource: "org.timberchain.TruckerData"
    action: DENY
}

// Marsheler permissions
rule MarshelerTimberPurchaseOrderPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseOrder"
    action: DENY
}

rule MarshelerDeliveryRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: ALL
    resource: "org.timberchain.DeliveryRequest"
    action: DENY
}

rule MarshelerTimberPurchaseRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseRequest"
    action: DENY
}

rule MarshelerPurchaseOrdersInvoicePermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: ALL
    resource: "org.timberchain.PurchaseOrdersInvoice"
    action: DENY
}

rule MarshelerMarshelerRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: UPDATE, DELETE
    resource: "org.timberchain.MarshelerRequest"
    action: DENY
}

rule MarshelerTimberRequestStatusPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: ALL
    resource: "org.timberchain.TimberRequestStatus"
    action: DENY
}

rule MarshelerTimbeterReadingPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: DELETE
    resource: "org.timberchain.TimbeterReading"
    action: DENY
}

rule MarshelerTruckerDataPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Marsheler"
    operation: ALL
    resource: "org.timberchain.TruckerData"
    action: DENY
}


// Seller permissions
rule SellerTimberPurchaseOrderPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseOrder"
    action: DENY
}

rule SellerDeliveryRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: ALL
    resource: "org.timberchain.DeliveryRequest"
    action: DENY
}

rule SellerTimberPurchaseRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseRequest"
    action: DENY
}

rule SellerPurchaseOrdersInvoicePermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: UPDATE, DELETE
    resource: "org.timberchain.PurchaseOrdersInvoice"
    action: DENY
}

rule SellerMarshelerRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: ALL
    resource: "org.timberchain.MarshelerRequest"
    action: DENY
}

rule SellerTimberRequestStatusPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: ALL
    resource: "org.timberchain.TimberRequestStatus"
    action: DENY
}

rule SellerTimbeterReadingPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: DELETE
    resource: "org.timberchain.TimbeterReading"
    action: DENY
}

rule SellerTruckerDataPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Seller"
    operation: ALL
    resource: "org.timberchain.TruckerData"
    action: DENY
}

// Trucker permissions
rule TruckerTimberPurchaseOrderPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseOrder"
    action: DENY
}

rule TruckerDeliveryRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: ALL
    resource: "org.timberchain.DeliveryRequest"
    action: DENY
}

rule TruckerTimberPurchaseRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: ALL
    resource: "org.timberchain.TimberPurchaseRequest"
    action: DENY
}

rule TruckerPurchaseOrdersInvoicePermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: ALL
    resource: "org.timberchain.PurchaseOrdersInvoice"
    action: DENY
}

rule TruckerMarshelerRequestPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: ALL
    resource: "org.timberchain.MarshelerRequest"
    action: DENY
}

rule TruckerTimberRequestStatusPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: ALL
    resource: "org.timberchain.TimberRequestStatus"
    action: DENY
}

rule TruckerTimbeterReadingPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: DELETE
    resource: "org.timberchain.TimbeterReading"
    action: DENY
}

rule TruckerTruckerDataPermission {
  	description: "Buyer Cannot delete any resorce"
    participant: "org.timberchain.Trucker"
    operation: UPDATE, DELETE
    resource: "org.timberchain.TruckerData"
    action: DENY
}



rule Default {
  description: "Grant business network administrators full access to resources"
    participant: "org.hyperledger.composer.system.Participant"
    operation: ALL
    resource: "org.timberchain.*"
    action: ALLOW
}

rule SystemACL {
  description: "Grant business network administrators full access to resources"
    participant: "org.hyperledger.composer.system.Participant"
    operation: ALL
    resource: "org.hyperledger.composer.system.**"
    action: ALLOW
}

rule NetworkAdminUser {
    description: "Grant business network administrators full access to user resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "**"
    action: ALLOW
}

rule NetworkAdminSystem {
    description: "Grant business network administrators full access to system resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "org.hyperledger.composer.system.**"
    action: ALLOW
}
